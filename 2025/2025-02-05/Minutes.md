# HDB++ Teleconference Meeting

Held on 2025/02/05 at 10:00 am CET on zoom.

**Framapad**: https://semestriel.framapad.org/p/hdbpp-meeting-20220518-9u9j

**Zoom link**: https://esrf.zoom.us/j/93141694731?pwd=elJTZDd6OUlaWEsrNGhBdkMyMzhZdz09

**Participants**:
- Sergi Rubio (ALBA)
- Graziano Scalamera (Elettra)
- Johan Forsberg (MAX IV)
- Reynald Bourtembourg (ESRF)
- Thomas Juerges (SKAO)
- Dmitry Egorov (Max IV)

**Next meeting date**:

# Minutes

## Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/hdbpp/meeting-minutes/-/blob/main/2024/2024-12-05/Minutes.md#summary-of-remaining-actions)

**All**:
- Test the sqlite backend https://gitlab.com/tango-controls/hdbpp/libhdbpp-sqlite (Write some tests using it?)
No progress
- Work on a strategy to benchmark the different backends, and their versions. https://gitlab.com/tango-controls/hdbpp/hdbpp-benchmark
No progress
- Share the tools which could be useful for the HDB++ community and update read the docs HDB++ section to advertise these tools.
No news
- Get feedback on the reader implementation in python3.

    Issues for authentification. We can enforce the use of the database backends security model.

    For further authentification it has to be discussed.

    The problem here is that authentification will be institute specific, we have to see if it can be completely handled by each facility, or if we need as well some work on the library as well.

    Let's focus on authentification, and secure the database access for write access first.

    And fix this issue for starter.

    https://gitlab.com/tango-controls/hdbpp/hdbpp-tickets/-/issues/2


**MaxIV**
- share the HDB++ Prometheus exporter: https://gitlab.com/tango-controls/hdbpp/hdbpp-exporter/

**Damien**:
- make public the summary confluence page of the cluster upgrade
not done
- Create a CONTRIBUTING.md file or something similar to describe the release process at least, and anything else that could be necessary. 
not done
- MR on libhdbpp-python to support decimation in timescaledb
not done
- Ping Guillaume from SKA on the next action
ping sent, no news, I guess we should move without him.

**Graziano, Reynald, Benjamin(?), Johan, Damien, Sergi**:
- Define a first simple CI test suite using hdbpp-benchmark

**Graziano**:
- Share the work done on supporting aggregates in MySQL and stored procedure to compute the time average.
  Currently available in Elettra Gitlab. To be moved to tango-controls/hdbpp.
  it's there:
  https://gitlab.com/tango-controls/hdbpp/libhdbpp-mysql

**Reynald**:
- Follow JTango Team to fix [tango-controls/JTango#140](https://gitlab.com/tango-controls/JTango/-/issues/140)
Reply from Guillaume Pichon: "We have scheduled it for March/April. It's also a blocking issue for us (Soleil)."
- Framadate for the next meeting and an online doc-camp.
Done

**Sergi**
- MR on lbhdbpp-python for a draft RFC on decimation.

    Sergi: I have a proposal for differentiate decimate and reduce, to be reviewed on the 26/27th february

- Share the work on the grafana libhdbpp plugin.

    it would be integrated in libhdbpp-python


**Dmitry**
- Create an issue on hdbpp-es to discriminate some errors that shouldn't appear as errors. https://gitlab.com/tango-controls/hdbpp/hdbpp-tickets/-/issues/6
 -> Dmitry to create an issue on hdbpp-es and start working on a fix.
  -> All collect the kind of errors that need to be stored better.
- Share some SQL queries to ease data extraction from timescaledb. Where should it go and in which format?
https://gitlab.com/tango-controls/hdbpp/hdbpp-timescale-project

**Johan**
- Create an issue to be able to deal with aliases https://gitlab.com/tango-controls/hdbpp/hdbpp-tickets/-/issues/5
Alba is using something similar, Sergi is to share what they did.

## News on recent developments

- We are trying to standardize a json format to be used by any archiver, not only tango:
https://github.com/jacomago/archiver_json_schema
it would be interesting to try and have an implementation.

- Improvements with yaml2archiving, it's now faster.
  Tagged version: 0.11.0
  https://gitlab.com/tango-controls/hdbpp/yaml2archiving/-/tags/0.11.0

## AOB 

- Documentation. Following the documentation workshop, we should re-organize hdb++ documentation into its own repo, or possibly several. Meeting planned 2025/02/06, 8.30am CET

- Tango meeting 2025
Giulianova in Italy, 21st to 23rd of May.

Previous HDB++ slides (from TM 2024 at Soleil): 
    - TJ: add permissions to Damien

HDB++ status report.

    -> Topics ongoing (RFC on decimation, authentication, documentation …)


- ICALEPCS 2025

- Issue with eventsubscriber, it's pushing a lot of att_parameter events at some point.

-Snap tools, what's there? Sergi said that Antonio(?) in MAX IV developed some tool based on MongoDB long time ago and ALBA created their own implementation and using it. But no one in MAX IV knows about it, so it was probably lost somewhere (no traces of "mongo" on the internal gitlab) 

## Summary of remaining actions

**All**:
- fix this issue not to expose credentials in plain text:
    https://gitlab.com/tango-controls/hdbpp/hdbpp-tickets/-/issues/2
- Test the sqlite backend https://gitlab.com/tango-controls/hdbpp/libhdbpp-sqlite (Write some tests using it?)
- Work on a strategy to benchmark the different backends, and their versions. https://gitlab.com/tango-controls/hdbpp/hdbpp-benchmark
- Share the tools which could be useful for the HDB++ community and update read the docs HDB++ section to advertise these tools.
- Get feedback on the reader implementation in python3.

**Johan**
- create an issue on cppTango so that we can retrieve if events are pushed by code client side.
https://gitlab.com/tango-controls/cppTango/-/issues/261

**TJ**
Pop up the google docs to get started on this year presentation for the Tango meetings.

**Damien**:
- make public the summary confluence page of the cluster upgrade
- Create a CONTRIBUTING.md file or something similar to describe the release process at least, and anything else that could be necessary. 
- MR on libhdbpp-python to support decimation in timescaledb

**Graziano, Reynald, Benjamin(?), Johan, Damien, Sergi**:
- Define a first simple CI test suite using hdbpp-benchmark

**Sergi**
- MR on lbhdbpp-python for a draft RFC on decimation.
- Share the work on the grafana libhdbpp plugin.
- Issue with aliases: https://gitlab.com/tango-controls/hdbpp/hdbpp-tickets/-/issues/5, share how it's handled at Alba

**Dmitry**
- Share some SQL queries to ease data extraction from timescaledb.

