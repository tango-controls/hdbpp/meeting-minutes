# HDB++ Teleconference Meeting

To be held on 2025/02/05 at 10:00 am CET on zoom.

**Framapad**: https://semestriel.framapad.org/p/hdbpp-meeting-20220518-9u9j

**Zoom link**: https://esrf.zoom.us/j/93141694731?pwd=elJTZDd6OUlaWEsrNGhBdkMyMzhZdz09

**Participants**:

**Next meeting date**:

# Minutes

## Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/hdbpp/meeting-minutes/-/blob/main/2024/2024-12-05/Minutes.md#summary-of-remaining-actions)

**All**:
- Test the sqlite backend https://gitlab.com/tango-controls/hdbpp/libhdbpp-sqlite (Write some tests using it?)
- Work on a strategy to benchmark the different backends, and their versions. https://gitlab.com/tango-controls/hdbpp/hdbpp-benchmark
- Share the tools which could be useful for the HDB++ community and update read the docs HDB++ section to advertise these tools.
- Get feedback on the reader implementation in python3.

**MaxIV**
- share the HDB++ Prometheus exporter

**Damien**:
- make public the summary confluence page of the cluster upgrade
- Create a CONTRIBUTING.md file or something similar to describe the release process at least, and anything else that could be necessary.
- MR on libhdbpp-python to support decimation in timescaledb
- Ping Guillaume from SKA on the next action

**Graziano, Reynald, Benjamin(?), Johan, Damien, Guillaume, Sergi**:
- Define a first simple CI test suite using hdbpp-benchmark

**Graziano**:
- Share the work done on supporting aggregates in MySQL and stored procedure to compute the time average.
  Currently available in Elettra Gitlab. To be moved to tango-controls/hdbpp.

**Reynald**:
- Follow JTango Team to fix [tango-controls/JTango#140](https://gitlab.com/tango-controls/JTango/-/issues/140)
- Framadate for the next meeting and an online doc-camp.

**Sergi**
- MR on lbhdbpp-python for a draft RFC on decimation.
- Share the work on the grafana libhdbpp plugin.

**Dmitry**
- Create an issue on hdbpp-es to discriminate some errors that shouldn't appear as errors.
- Share some SQL queries to ease data extraction from timescaledb.

**Johan**
- Create an issue to be able to deal with aliases

## News on recent developments

- We are trying to standardize a json format to be used by any archiver, not only tango:
https://github.com/jacomago/archiver_json_schema
it would be interesting to try and have an implementation.

## AOB

- Documentation. Following the documentation workshop, we should re-organize hdb++ documentation into its own repo, or possibly several. Meeting planned 2025/02/06, 8.30am CET

## Summary of remaining actions
