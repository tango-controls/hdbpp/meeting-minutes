# HDB++ Teleconference Meeting

To be held on 2024/12/05 at 10:00 am CET on zoom.

**Framapad**: https://semestriel.framapad.org/p/hdbpp-meeting-20220518-9u9j

**Zoom link**: https://esrf.zoom.us/j/93141694731?pwd=elJTZDd6OUlaWEsrNGhBdkMyMzhZdz09

**Participants**:

**Next meeting date**:

# Minutes

## Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/hdbpp/meeting-minutes/-/blob/master/2024/2024-04-19/Minutes.md#summary-of-remaining-actions)

**All**:
- Test the sqlite backend https://gitlab.com/tango-controls/hdbpp/libhdbpp-sqlite (Write some tests using it?)
- Work on a strategy to benchmark the different backends, and their versions. https://gitlab.com/tango-controls/hdbpp/hdbpp-benchmark
- Share the tools which could be useful for the HDB++ community and update read the docs HDB++ section to advertise these tools.
- Get feedback on the reader implementation in python3.


**Graziano, Reynald, Benjamin(?), Johan, Damien, Guillaume, Sergi**:
- Define a first simple CI test suite using hdbpp-benchmark


**Damien**:
- make public the summary confluence page of the cluster upgrade
- Create a CONTRIBUTING.md file or something similar to describe the release process at least, and anything else that could be necessary.
- MR on libhdbpp-python to support decimation in timescaledb

**Graziano**:
- Share the work done on supporting aggregates in MySQL and stored procedure to compute the time average.


**Reynald**:
- Ping JTango Team to fix [tango-controls/JTango#140](https://gitlab.com/tango-controls/JTango/-/issues/140)


**Sergi**
- MR on libhdbpp-python to be able to choose the format for the time data. Default to datetime()
- MR on lbhdbpp-python for a draft RFC on decimation.

## News on recent developments

- Grafana integration based on libhdbpp-python
- libhdbpp supports tango 9.3 to 10.0, with support for mac and linux, windows support is almost ready.

## AOB

- Documentation. Following the documentation workshop, we should re-organize hdb++ documentation into its own repo, or possibly several.

- Status of https://gitlab.com/tango-controls/hdbpp/HdbConfiguratorServer. It is used at the ESRF, but could be integrated into hdbpp-cm. Broader discussion on hdbpp-cm, to see if it could be improved for a better integration with yaml2archiving or other tools.

## Summary of remaining actions
