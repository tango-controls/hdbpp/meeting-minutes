# HDB++ Teleconference Meeting

Held on 2024/12/05 at 10:00 am CET on zoom.

**Participants**:

- Reynald Bourtembourg (ESRF)
- Damien Lacoste (ESRF)
- Graziano Scalamera (Elettra)
- Johan Forsberg (MAX IV)
- Dmitry Egorov (MAX IV)
- Sergi Rubio (ALBA)

**Next meeting date**:


# Minutes
## Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/hdbpp/meeting-minutes/-/blob/master/2024/2024-04-19/Minutes.md#summary-of-remaining-actions)
**All**:
- Test the sqlite backend https://gitlab.com/tango-controls/hdbpp/libhdbpp-sqlite (Write some tests using it?)
- Work on a strategy to benchmark the different backends, and their versions. https://gitlab.com/tango-controls/hdbpp/hdbpp-benchmark


- Share the tools which could be useful for the HDB++ community and update read the docs HDB++ section to advertise these tools.

> *Action - Dmitry*: share and document how to do some SQL queries (used with Grafana for instance).
> *Action - MaxIV*: share the HDB++ Prometheus exporter
> Sergi  is showing a demo of tango_browser (already in tango-controls/hdbpp gitlab group).
> Sergi is showing a demo of a grafana plugin able to use pyhdbpp for the data extraction (advantage being to be able to retrieve and display data from serveral data sources using different HDB++ schemas). Sergi will contact José Ramos to get more details on how the plugin was written.
This plugin will be made public and documented.

> Dmitry: it would be nice to get the possibility to have user defined aliases for some attributes stored in HDB++. Sergi is showing an example of ArchivingAliasNames.csv file used by their multidb feature. It's possible to define an alias for an index of an array.
> Action - Johan: Create an issue to be able to deal with aliases


- Get feedback on the reader implementation in python3.


**Graziano, Reynald, Benjamin(?), Johan, Damien, Guillaume, Sergi**:
- Define a first simple CI test suite using hdbpp-benchmark
> Action - Damien: Ping Guillaume from SKA


**Damien**:
- make public the summary confluence page of the cluster upgrade
- Create a CONTRIBUTING.md file or something similar to describe the release process at least, and anything else that could be necessary.
- MR on libhdbpp-python to support decimation in timescaledb

> No progress

**Graziano**:
- Share the work done on supporting aggregates in MySQL and stored procedure to compute the time average.
> Currently available in Elettra Gitlab. To be moved to tango-controls/hdbpp by Graziano.

**Reynald**:
- Ping JTango Team to fix [tango-controls/JTango#140](https://gitlab.com/tango-controls/JTango/-/issues/140)
> Ping done


**Sergi**
- MR on libhdbpp-python to be able to choose the format for the time data. Default to datetime()
> Merged
- MR on lbhdbpp-python for a draft RFC on decimation.
> Still pending

## News on recent developments

- Grafana integration based on libhdbpp-python
> Documentation will be written and this will be added to libhdbpp-python or will be in a separated project
- Dmitry is updating the Aggregate timescaledb scripts 
- libhdbpp supports tango 9.3 to 10.0, with support for mac and linux, windows support is almost ready.
- Benjamin built some HDB++ conda packages using Tango v10.
Damien: we should add some CICD tests to ensure HDB++ works with all Tango versions.
- yaml2archiving tool is now talking to the TANGO DB directly to configure the events and polling.

## AOB

- Documentation. Following the documentation workshop, we should re-organize hdb++ documentation into its own repo, or possibly several.

We will organize a HDB++ online doc camp (1/2 day) around February. 
Reynald will send a Framadate.

- Status of https://gitlab.com/tango-controls/hdbpp/HdbConfiguratorServer. It is used at the ESRF, but could be integrated into hdbpp-cm. Broader discussion on hdbpp-cm, to see if it could be improved for a better integration with yaml2archiving or other tools.

Sergi: All tools should be database-agnostic.

Dmitry: When there is an error reading an attribute (like "It is currently not allowed to read attribute"), it appears as NoK. This is confusing because the archiving subscription is actually working as expected. It would be good to not consider these kinds of errors.
> Dmitry to write an issue about that.

## Next HDB++ Meeting

We aim for beginning of February.
Reynald will send a Framadate poll.

## Summary of remaining actions
**All**:
- Test the sqlite backend https://gitlab.com/tango-controls/hdbpp/libhdbpp-sqlite (Write some tests using it?)
- Work on a strategy to benchmark the different backends, and their versions. https://gitlab.com/tango-controls/hdbpp/hdbpp-benchmark
- Share the tools which could be useful for the HDB++ community and update read the docs HDB++ section to advertise these tools.

- Get feedback on the reader implementation in python3.

**MaxIV**
- share the HDB++ Prometheus exporter

**Damien**:
- make public the summary confluence page of the cluster upgrade
- Create a CONTRIBUTING.md file or something similar to describe the release process at least, and anything else that could be necessary.
- MR on libhdbpp-python to support decimation in timescaledb
- Ping Guillaume from SKA on the next action

**Graziano, Reynald, Benjamin(?), Johan, Damien, Guillaume, Sergi**:
- Define a first simple CI test suite using hdbpp-benchmark

**Graziano**:
- Share the work done on supporting aggregates in MySQL and stored procedure to compute the time average.
  Currently available in Elettra Gitlab. To be moved to tango-controls/hdbpp.

**Reynald**:
- Follow JTango Team to fix [tango-controls/JTango#140](https://gitlab.com/tango-controls/JTango/-/issues/140)
- Framadate for the next meeting and an online doc-camp.

**Sergi**
- MR on lbhdbpp-python for a draft RFC on decimation.
- Share the work on the grafana libhdbpp plugin.

**Dmitry**
- Create an issue on hdbpp-es to discriminate some errors that shouldn't appear as errors.
- Share some SQL queries to ease data extraction from timescaledb.

**Johan**
- Create an issue to be able to deal with aliases
