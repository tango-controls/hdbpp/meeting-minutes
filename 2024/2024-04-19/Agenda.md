# HDB++ Teleconference Meeting

To be held on 2024/04/09 at 10:00 am CEST on zoom.

**Framapad**: https://semestriel.framapad.org/p/hdbpp-meeting-20220518-9u9j

**Zoom link**: https://esrf.zoom.us/j/93141694731?pwd=elJTZDd6OUlaWEsrNGhBdkMyMzhZdz09

**Participants**:

**Next meeting date**:

# Minutes

## Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/hdbpp/meeting-minutes/-/blob/master/2024/2024-02-06/Minutes.md#summary-of-remaining-actions)

**All**:
- Test the sqlite backend https://gitlab.com/tango-controls/hdbpp/libhdbpp-sqlite (Write some tests using it?)
- Work on a strategy to benchmark the different backends, and their versions. https://gitlab.com/tango-controls/hdbpp/hdbpp-benchmark
- Share the tools which could be useful for the HDB++ community and update read the docs HDB++ section to advertise these tools.
- Get feedback on the reader implementation in python3.
- Answer to the poll for the global archiving collaboration online meeting:  
  https://evento.renater.fr/survey/control-systems-archiving-collaboration-ttaalz0p

**Graziano, Reynald, Benjamin(?), Johan, Damien(?)**:
- Define a first simple CI test suite using hdbpp-benchmark

**Damien**:
- make public the summary confluence page of the cluster upgrade

**Graziano**:
- Share the work done on supporting aggregates in MySQL and stored procedure to compute the time average.

**Reynald**:
- Ping JTango Team to fix [tango-controls/JTango#140](https://gitlab.com/tango-controls/JTango/-/issues/140)

**Action - Damien**:
Create a CONTRIBUTING.md file or something similar to describe the release process at least, and anything else that could be necessary.

## News on recent developments

- Benchmark results from Guillaume Jourjon (SKAO) on their archiving solution.

## AOB

- TANGO community meeting May 28-30. Do we present something for HDB++?

- Archiving collaboration. Meeting to be held April 30th 5pm CEST on zoom. This meeting will be dedicated to a presentation of EPICS archiver appliance solutions.

## Summary of remaining actions
