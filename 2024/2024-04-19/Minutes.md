# HDB++ Teleconference Meeting

Held on 2024/04/19 at 10:00 am CEST on zoom.

**Participants**:

Damien Lacoste (ESRF)
Graziano Scalamera (ELETTRA)
Guillaume Jourjon (SKAO)
Sergi Rubio (ALBA)
Johan Forsberg (MaxIV)
Thomas Juerges (SKAO)

**Next meeting date**:
    May 17th 10 am CEST

# Minutes

## Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/hdbpp/meeting-minutes/-/blob/master/2024/2024-02-06/Minutes.md#summary-of-remaining-actions)

**All**:
- Test the sqlite backend https://gitlab.com/tango-controls/hdbpp/libhdbpp-sqlite (Write some tests using it?)

    TJ made it work on MacOS. MR?

- Work on a strategy to benchmark the different backends, and their versions. https://gitlab.com/tango-controls/hdbpp/hdbpp-benchmark
- Share the tools which could be useful for the HDB++ community and update read the docs HDB++ section to advertise these tools.
- Get feedback on the reader implementation in python3.

    We currently have mariadb implementation returning time as time() and timescaledb as datetime() we should standardize it.

    We'll make an argument for the reader to choose the type we want to use. The default is datetime(). Sergi to provide MR.


**Graziano, Reynald, Benjamin(?), Johan, Damien(?)**:
- Define a first simple CI test suite using hdbpp-benchmark

**Damien**:
- make public the summary confluence page of the cluster upgrade
Not done

**Graziano**:
- Share the work done on supporting aggregates in MySQL and stored procedure to compute the time average.
Not ready yet, it's working, to be presented later on.

**Reynald**:
- Ping JTango Team to fix [tango-controls/JTango#140](https://gitlab.com/tango-controls/JTango/-/issues/140)
No news?

**Action - Damien**:
Create a CONTRIBUTING.md file or something similar to describe the release process at least, and anything else that could be necessary.
Not done

## News on recent developments
libhdbpp-python

    Sergi is working on parrallel queries.

    Decimation has to be implemented in timescaledb backend. Damien to work on the implementation before July.

    Sergi to write a draft RFC on decimation in libhdbpp-python based on what is done in mariadb.


- Benchmark results from Guillaume Jourjon (CSIRO/SKAO) on their archiving solution.
  - presentation available at https://docs.google.com/presentation/d/1eSoKwk-McSMha8cV4yimTp7xSp_Gpu-9we5eEA2zwmw/edit#slide=id.p1
    - Benchmarking code available at https://gitlab.com/ska-telescope/ska-eda-perf-eval/ 

## AOB

- TANGO community meeting May 28-30. Do we present something for HDB++?
Status report
Standardized number collection between the institutes. The link will be shared by email, please fill it in.

- Archiving collaboration. Meeting to be held April 30th 5pm CEST on zoom. This meeting will be dedicated to a presentation of EPICS archiver appliance solutions.

## Summary of remaining actions
**All**:
- Test the sqlite backend https://gitlab.com/tango-controls/hdbpp/libhdbpp-sqlite (Write some tests using it?)
- Work on a strategy to benchmark the different backends, and their versions. https://gitlab.com/tango-controls/hdbpp/hdbpp-benchmark
- Share the tools which could be useful for the HDB++ community and update read the docs HDB++ section to advertise these tools.
- Get feedback on the reader implementation in python3.


**Graziano, Reynald, Benjamin(?), Johan, Damien, Guillaume, Sergi**:
- Define a first simple CI test suite using hdbpp-benchmark


**Damien**:
- make public the summary confluence page of the cluster upgrade
- Create a CONTRIBUTING.md file or something similar to describe the release process at least, and anything else that could be necessary.
- MR on libhdbpp-python to support decimation in timescaledb


**Graziano**:
- Share the work done on supporting aggregates in MySQL and stored procedure to compute the time average.


**Reynald**:
- Ping JTango Team to fix [tango-controls/JTango#140](https://gitlab.com/tango-controls/JTango/-/issues/140)


**Sergi**
- MR on libhdbpp-python to be able to choose the format for the time data. Default to datetime()
- MR on lbhdbpp-python for a draft RFC on decimation.


**TJ**
- MR on libhdbpp-sqlite for MacOS support
Done. MR: https://gitlab.com/tango-controls/hdbpp/libhdbpp-sqlite/-/merge_requests/1


