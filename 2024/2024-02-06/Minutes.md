# HDB++ Teleconference Meeting

Held on 2024/02/06 at 3:00 pm CET on zoom.

**Framapad**: https://semestriel.framapad.org/p/hdbpp-meeting-20220518-9u9j

**Participants**:
- Graziano Scalamera (Elettra)
- Johan Forsberg (MaxIV)
- Reynald Bourtembourg (ESRF)

**Next meeting date**:  Tuesday 9th April at 15:00 CEST  
**Please protest** if it's not good for you!

# Minutes

## Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/hdbpp/meeting-minutes/-/blob/main/2023/2023-12-05/Minutes.md?ref_type=heads#summary-of-remaining-actions)

Action - All:

    Test the sqlite backend https://gitlab.com/tango-controls/hdbpp/libhdbpp-sqlite

> What about writing some tests using this backend?

Action - All:

    Work on a strategy to benchmark the different backends, and their versions. https://gitlab.com/tango-controls/hdbpp/hdbpp-benchmark

> Performance or regression tests?  
> We should start by defining what do we want to test.  
> What backend, version, metrics (insertion rate, time, .disk usage, ..)?  
> For regression tests, maybe we could have a nightly CI job?  
> We should probably start with defining a good test suite?  
**Action - Graziano, Reynald, Benjamin(?), Johan, Damien(?)**: Define a first simple CI test suite using hdbpp-benchmark

Action - JTango team:

    Fix tango-controls/JTango#140 (https://gitlab.com/tango-controls/JTango/-/issues/140)

> Reynald will ping Gwen

Action - Damien:
make public the summary confluence page of the cluster upgrade

    > Nothing to report

Action - All:
Share the tools which could be useful for the HDB++ community and update read the docs HDB++ section to advertise these tools.
> Graziano has been working on supporting aggregates in MySQL. He has a stored procedure to compute the time average.

Action - All:
Get feedback on the reader implementation in python3.

> An [issue](https://gitlab.com/tango-controls/hdbpp/libhdbpp-python/-/issues/6) and a [MR](https://gitlab.com/tango-controls/hdbpp/libhdbpp-python/-/merge_requests/10)
> were created to ask for having a way to sort data per timestamp (oldest data first).

Action - Damien:
Create a CONTRIBUTING.md file or something similar to describe the release process at least, and anything else that could be necessary.
> Nothing to report

Action - Damien:

    Invite everyone to the global archiving collaboration. Contact Gianluca Chiozzi on this subject.
    Gianluca was contacted but no answer.
    For the rest there will be an online meeting in february, we have a poll for the date:

        https://evento.renater.fr/survey/control-systems-archiving-collaboration-ttaalz0p


## News on recent developments

- Cpptango is moving towards 10.0, with adoption of c++17 standard, and use of Cmake find modules to find dependencies.
> It's maybe a bit too early to move to using TangoCmakeModules git submodule. 
> Let's rediscuss this point during the next meetings

- Support for aliases in HDB++
> It would be nice to support attribute/device aliases in HDB++

- We have some work to do: tango-controls/TangoTickets#102 ?

- Max IV: All numeric data at MaxIV has been migrated from Cassandra to TimescaleDB

It would be nice to be able to rename easily the attributes in HDB++.    
Johan: it would be nice to link several attributes names with the same HDB++ data.    
This could be done via an additional table. 


## Any other business


## Summary of remaining actions

**All**:
- Test the sqlite backend https://gitlab.com/tango-controls/hdbpp/libhdbpp-sqlite (Write some tests using it?)
- Work on a strategy to benchmark the different backends, and their versions. https://gitlab.com/tango-controls/hdbpp/hdbpp-benchmark
- Share the tools which could be useful for the HDB++ community and update read the docs HDB++ section to advertise these tools.
- Get feedback on the reader implementation in python3.
- Answer to the poll for the global archiving collaboration online meeting:  
  https://evento.renater.fr/survey/control-systems-archiving-collaboration-ttaalz0p

**Graziano, Reynald, Benjamin(?), Johan, Damien(?)**:
- Define a first simple CI test suite using hdbpp-benchmark

**Damien**:
- make public the summary confluence page of the cluster upgrade

**Graziano**:
- Share the work done on supporting aggregates in MySQL and stored procedure to compute the time average.

**Reynald**:
- Ping JTango Team to fix [tango-controls/JTango#140](https://gitlab.com/tango-controls/JTango/-/issues/140)
