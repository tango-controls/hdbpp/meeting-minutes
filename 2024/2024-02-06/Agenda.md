# HDB++ Teleconference Meeting

To be held on 2024/02/06 at 3:00 pm CET on zoom.

**Framapad**: https://semestriel.framapad.org/p/hdbpp-meeting-20220518-9u9j

**Zoom link**: https://esrf.zoom.us/j/93141694731?pwd=elJTZDd6OUlaWEsrNGhBdkMyMzhZdz09

**Participants**:

**Next meeting date**:

# Minutes

## Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/hdbpp/meeting-minutes/-/blob/master/2023/2023-12-05/Minutes.md#summary-of-remaining-actions)

**Action - All**:
- Test the sqlite backend
https://gitlab.com/tango-controls/hdbpp/libhdbpp-sqlite

**Action - All**:
- Work on a strategy to benchmark the different backends, and their versions.
https://gitlab.com/tango-controls/hdbpp/hdbpp-benchmark

**Action - JTango team**:
- Fix https://gitlab.com/tango-controls/JTango/-/issues/140

**Action - Damien**:
make public the summary confluence page of the cluster upgrade

**Action - All**:
Share the tools which could be useful for the HDB++ community and update read the docs HDB++ section to advertise these tools.

**Action - All**:
Get feedback on the reader implementation in python3.

**Action - Damien**:
Create a CONTRIBUTING.md file or something similar to describe the release process at least, and anything else that could be necessary.

**Action - Damien**:
Invite everyone to the global archiving collaboration.
Contact Gianluca Chiozzi on this subject.

## News on recent developments

- Cpptango is moving towards 10.0, with adoption of c++17 standard, and use of Cmake find modules to find dependencies.

## AOB

- Support for aliases in HDB++

- We have some work to do: https://gitlab.com/tango-controls/TangoTickets/-/issues/102

## Summary of remaining actions
