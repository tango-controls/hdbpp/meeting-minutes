# HDB++ Teleconference Meeting - 2021/11/17

To be held on 2021/11/17 at 10:00 CET on zoom.

# Agenda
 
 * Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/hdbpp/meeting-minutes/-/blob/master/2021/2021-07-23/Minutes.md#summary-of-remaining-actions)
 * News on recent developments
 * High priority issues
 * Could we make hdbpp-cm optional (or even get rid of it)?
 * Create a framework to allow and manage data post-processing in hdb++
 * AOB
