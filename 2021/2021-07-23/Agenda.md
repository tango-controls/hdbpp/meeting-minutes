# HDB++ Teleconference Meeting - 2021/07/23

To be held on 2021/07/23 at 11:00 CEST on zoom.

# Agenda
 
 1. Status of [Actions defined in the previous meetings](https://github.com/tango-controls-hdbpp/meeting-minutes/blob/master/2021/2021-05-19/Minutes.md#summary-of-remaining-actions)
 2. News on recent developments
 3. High priority issues
 4. AOB
