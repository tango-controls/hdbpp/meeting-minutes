# Tango HDB++ collaboration Teleconf Meeting - 2022/05/18
Held on 2022/05/18 on Zoom.  
**Framapad**: https://semestriel.framapad.org/p/hdbpp-meeting-20220518-9u9j?lang=en

**Participants**:   
    - Damien Lacoste (ESRF)  
    - Graziano Scalamera (ELETTRA)  
    - Johan Forsberg (Max IV)  
    - Lorenzo Pivetta (ELETTRA)  
    - Reynald Bourtembourg (ESRF)  
    - Sergi Rubio (ALBA)  
    - Thomas Juerges (SKAO)
    
# Minutes
    
## Status of [Actions defined in the previous meetings](https://github.com/tango-controls-hdbpp/meeting-minutes/blob/master/2021/2021-07-23/Minutes.md#summary-of-remaining-actions)
    
**Action - Sergi, Damien, Lorenzo, Max IV, Giacomo**:   
Sergi should guide Damien in implementing support for TimescaleDB in pytangoarchiving.  
Sergi should review the PR created by Damien on pytangoarchiving (https://github.com/tango-controls/PyTangoArchiving/pull/18).  
Max IV is invited to review https://github.com/tango-controls/PyTangoArchiving/pull/18 which is a proposal from Damien for an AbstractReader.  
Sergi will write the MySQL backend and review and approve https://github.com/tango-controls/PyTangoArchiving/pull/18.  
> Extraction moved to libhdbpp-python (https://gitlab.com/tango-controls/hdbpp/libhdbpp-python).  

Sergi will share the work with MaxIV for support with Cassandra.  
> abandonned  

Sergi will contact Damien and Giacomo to try to get a unified API (python)  
> WIP
        
**Action Graziano & Matteo**: Converge on a solution for a script to configure easily a bunch of attributes from a text configuration file and push this solution to tango-controls-hdbpp Github organization. MaxIV is using some template yml files to configure easily the archiving settings (event thresholds for specific classes, strategies)  
> MaxIV to publish its yaml tool to do so, but it's not entirely ready yet.
    
    
**Action - Alba**: Provide MariaDB container(s) for the HDB++ benchmarking suite. Sergi will get in touch with Graziano on that topic.
    
**Action - Sergi**: Create an issue on hdbpp-es (if there is none yet) about the attributes which are sometimes not archived even though they are configured with the ALWAYS archiving strategy.  
> Some problems are still present when moving devices from one host to another or to another device server instance. This is due to the fact that the archiver needs to contact the admin device to manage the event subscription and there is still an issue in cppTango in case the admin device is renamed.  
    It is still not clear whether there is indeed an event reconnection issue with latest cppTango when moving a device server from one host to another. => Needs to be tested. 
    See https://gitlab.com/tango-controls/cppTango/-/merge_requests/737 for a fix proposal in cppTango.
    
**Action - MaxIV**: Transfer the YAML tools to tango-controls Gitlab HDB++ subgroup to share them with the community and to help to get some feedback from the community.
    
**Action - Sergi**: Create an issue in libhdbpp-mysql about the compilation problem on Debian 10.
    
**Action - Sergi**: Publish a script which is monitoring and restarting automatically archiving on attributes in error.  
> Script is available here: https://gitlab.com/tango-controls/hdbpp/libhdbpp-pytangoarchiving/-/blob/master/PyTangoArchiving/check.py

**Action - All**: Share the tools which could be useful for the HDB++ community and update read the docs HDB++ section to advertise these tools.

**Action - Reynald**: Start migration to gitlab.
> New attempt this week.

## General discussions
    
Still problems on compiling libhdb++ on debian 10
     
### Tango Collaboration meeting end of june:
**Action - Sergi, Johan, Lucio**: Prepare a general talk and a short tour of the tools around hdb++ (Demo or Video Demo if possible):
- python extraction.
- eGiga
- archwizard
- ...

## News on recent developments
    
MAX IV web based HDB++ monitoring tool "archwizard" is available at https://gitlab.com/tango-controls/hdbpp/archwizard This is a mirrored internal repository for now.
    
libhdbpp-python  https://gitlab.com/tango-controls/hdbpp/libhdbpp-python Timescaledb reader implementation merged.  
The python extraction library is now compatible with mariadb and timescaledb
    
Deadlock fixed on hdbpp-es.

Support for images in libhdbpp-timescale. To be further tested and benchmarked, maybe.

**Action - All**: Try the reader implementation in python3.

## AOB

### Next HDB++ Teleconf Meeting
The next HDB++ meeting will take place on Wednesday 8th June at 10:00 CEST. 

## Summary of remaining actions

**Action - Sergi, Damien, Giacomo**:   
Sergi will contact Damien and Giacomo to try to get a unified extraction API (python)  
        
**Action Graziano & Matteo**: Converge on a solution for a script to configure easily a bunch of attributes from a text configuration file and push this solution to tango-controls-hdbpp Github organization. MaxIV is using some template yml files to configure easily the archiving settings (event thresholds for specific classes, strategies)
    
**Action - Alba**: Provide MariaDB container(s) for the HDB++ benchmarking suite. Sergi will get in touch with Graziano on that topic.
    
**Action - cppTango team**: 
- Test the event reconnection issue when moving a device server instance from one host to another. Is this issue still present in recent cppTango development branches?
- Review and merge https://gitlab.com/tango-controls/cppTango/-/merge_requests/737 when ready
    
**Action - MaxIV**: Transfer the YAML tools to tango-controls Gitlab HDB++ subgroup to share them with the community and to help to get some feedback from the community.  
    
**Action - Sergi**: Create an issue in libhdbpp-mysql about the compilation problem on Debian 10.  

**Action - All**: Share the tools which could be useful for the HDB++ community and update read the docs HDB++ section to advertise these tools.

**Action - Reynald**: Finish migration to gitlab.

**Action - Sergi, Johan, Lucio**: Prepare a general talk and a short tour of the tools around hdb++ (Demo or Video Demo if possible):
- python extraction.
- eGiga
- archwizard
- ...

**Action - All**: Try the reader implementation in python3.