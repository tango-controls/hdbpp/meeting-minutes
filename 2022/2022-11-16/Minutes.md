# HDB++ Meeting

held on 2022/11/16-17 at ASTRON and on zoom.

**Participants**:

**Next meeting date**:
    19th of january 10 CET


# Minutes

## Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/hdbpp/meeting-minutes/-/blob/master/2022/2022-09-29/Minutes.md#summary-of-remaining-actions)

**Action Graziano & Matteo**:
 Converge on a solution for a script to configure easily a bunch of attributes from a text configuration file and push this solution to tango-controls-hdbpp Github organization.
 MaxIV is using some template yml files to configure easily the archiving settings (event thresholds for specific classes, strategies)

> I think we're talking about this: https://gitlab.com/tango-controls/hdbpp/yaml2archiving

**Action - Sergi, Graziano**:
 Check if SKA provide MariaDB container(s) for the HDB++ benchmarking suite.
> It doesn't apply anymore

**Action - cppTango team**:
- Test the event reconnection issue when moving a device server instance from one host to another. Is this issue still present in recent cppTango development branches?
- Review and merge https://gitlab.com/tango-controls/cppTango/-/merge_requests/737 when ready
> Not done yet

**Action - Sergi**:
 Create a MR, if needed in libhdbpp-mysql about the compilation problem on Debian 10.
> It doesn't apply anymore.

**Action - Sergi**:
    Make a poll for the name of the python extraction library
      - pyhdbpp (.reader + .config)
      - libhdbpp
      - hdbpp (already used by https://github.com/dvjdjvu/hdbpp, hdb++/postgres api)
      - hdbpy
      - hdbreader / hdbconfig
> I think we agreed to stay on the same pyhdbpp

**Action - Damien**:
    make public the summary confluence page of the cluster upgrade
> Not done

**Action - Damien**:
    Tag and release hdbpp-es / hdbpp-cm / libhdbpp / libhdbpp-timescale
> Done

**Action - All**:
    Share the tools which could be useful for the HDB++ community and update read the docs HDB++ section to advertise these tools.

**Action - All**:
    Try the reader implementation in python3.


## News on recent developments

## AOB

 - TimescaleDB license is not open source. We should make sure we are safe to use it.
 - There is a snapshot tool with different flavours (java and python) that generated some interest.
 Maybe we could put some work into it to make it even better, integrate it with hdbpp?
 - There is a need for a sqlite backend implementation that could be used for testing purposes.
 - We should set up a benchmarking platform, based on the work from Graziano, to be able to compare different backends,
 and versions of the backends for hdbpp.

## Summary of remaining actions

**Action - Thomas Juerges**:
- Ask for some legal advice on the timescaleDB license.

**Action - All**:
- 15 min SQLite backend challenge: Can we build an sqlite backend for hdbpp in less than 15 min?
Any contributions that took longer, but did it anyway will be accepted.

**Action - All**:
- Work on a strategy to benchmark the different backends, and their versions.

**Action - cppTango team**:
- Test the event reconnection issue when moving a device server instance from one host to another. Is this issue still present in recent cppTango development branches?
- Review and merge https://gitlab.com/tango-controls/cppTango/-/merge_requests/737 when ready

**Action - Damien**:
    make public the summary confluence page of the cluster upgrade

**Action - All**:
    Share the tools which could be useful for the HDB++ community and update read the docs HDB++ section to advertise these tools.

**Action - All**:
    Try the reader implementation in python3.
