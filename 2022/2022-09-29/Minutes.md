# HDB++ Teleconference Meeting

held on 2022/09/29 at 10:00 CET on zoom.

**Framapad**: https://semestriel.framapad.org/p/hdbpp-meeting-20220518-9u9j

**Participants**:
    - Benjamin Bertrand (Max IV)
    - Damien Lacoste (ESRF)
    - Dmitry Egorov (CFEL)
    - Graziano Scalamera (Elettra)
    - Jan David Mol (Astron)
    - Johan Forsberg (Max IV)
    - Sergi Rubio (ALBA)
    - Reynald Bourtembourg (ESRF)
    - Stefano Di Frischia (INAF)
    - Thomas Juerges (SKAO)

**Next meeting date**:
    24th of november 10 CET unless we meet at
    SIG meeting in the netherlands 

# Minutes

## Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/hdbpp/meeting-minutes/-/blob/master/2022/2022-05-18/Minutes.md#news-on-recent-developments)

**Action - Sergi, Damien, Giacomo**:
 Sergi will contact Damien and Giacomo to try to get a unified extraction API (python)  
> Done

**Action Graziano & Matteo**:
 Converge on a solution for a script to configure easily a bunch of attributes from a text configuration file and push this solution to tango-controls-hdbpp Github organization.
 MaxIV is using some template yml files to configure easily the archiving settings (event thresholds for specific classes, strategies)
> No news

**Action - Alba**:
 Provide MariaDB container(s) for the HDB++ benchmarking suite. Sergi will get in touch with Graziano on that topic.
> Check if SKA could have such container(s) available

**Action - cppTango team**: 
- Test the event reconnection issue when moving a device server instance from one host to another. Is this issue still present in recent cppTango development branches? 
- Review and merge https://gitlab.com/tango-controls/cppTango/-/merge_requests/737 when ready
>  Planned for cppTango 9.4.1 (Before April 2023)

**Action - MaxIV**: 
 Transfer the YAML tools to tango-controls Gitlab HDB++ subgroup to share them with the community and to help to get some feedback from the community.
> yaml2archiving is now on HDB++'s Gitlab: https://gitlab.com/tango-controls/hdbpp/yaml2archiving
    
**Action - Sergi**:
 Create an issue in libhdbpp-mysql about the compilation problem on Debian 10. 
> Sergi: It is fixed.
> Thomas: How about tagging this working version?
> Sergi: We added one minor modification to the CMakefile.txt though.

**Action - All**:
 Share the tools which could be useful for the HDB++ community and update read the docs HDB++ section to advertise these tools.
> Johan: archwizard - display archive config
> archviewer - display TimescaleDB archive content in a GUI
> Sergi talked about a taurus widget using the python extraction library.

**Action - Reynald**: 
 Finish migration to gitlab.  
> Done

**Action - Sergi, Johan, Lucio**:
 For the june tango meeting, prepare a general talk and a short tour of the tools around hdb++ (Demo or Video Demo if possible):
  - python extraction.
  - eGiga
  - archwizard
  - ...
> Done

**Action - All**: 
 Try the reader implementation in python3.

## News on recent developments

  - Fixed an issue on hdbpp-es. On errors on batch insertion retry was implemented in the backend and hdbpp-es, causing some errors.
  - At the ESRF, cluster update from postgresql-11/timescaledb-1.7 to postgresql-12/timescaledb-2.7.2
    - tedious process…
    - first update old cluster, using pg_upgrade for postgres and the normal procedure for timescale.
    - then integrate one of the machine of the new cluster.
    - switch to this machine as primary, and add the other machines as replicas.
    - drop the old cluster
    - a fast network is a good idea!

  -libhdbpp-python to be renamed pyhdb? (pyhdb is already taken on pypi)
  - pyhdb.reader store configuration as a free property in tango


  - Demo from Dmitry of grafana as a viewer
## AOB

 - hdbpp python reader ready for release
 - New release of hdbpp-es / hdbpp-cm / libhdbpp / libhdbpp-timescale? Current tag is 2 years old.
 - Publish package on conda-forge. Naming?
  
 - SIG meeting on HDB in the netherlands
   https://framadate.org/1DtV6YKWtVFWnIhc

- MaxIV: migration in progress.
  1 beamline at a time

## Summary of remaining actions
**Action Graziano & Matteo**:
 Converge on a solution for a script to configure easily a bunch of attributes from a text configuration file and push this solution to tango-controls-hdbpp Github organization.
 MaxIV is using some template yml files to configure easily the archiving settings (event thresholds for specific classes, strategies)

**Action - Sergi, Graziano**:
 Check if SKA provide MariaDB container(s) for the HDB++ benchmarking suite.

**Action - cppTango team**: 
- Test the event reconnection issue when moving a device server instance from one host to another. Is this issue still present in recent cppTango development branches? 
- Review and merge https://gitlab.com/tango-controls/cppTango/-/merge_requests/737 when ready

**Action - Sergi**:
 Create a MR, if needed in libhdbpp-mysql about the compilation problem on Debian 10.

**Action - Sergi**:
    Make a poll for the name of the python extraction library
      - pyhdbpp (.reader + .config)
      - libhdbpp
      - hdbpp (already used by https://github.com/dvjdjvu/hdbpp, hdb++/postgres api)
      - hdbpy 
      - hdbreader / hdbconfig

**Action - Damien**:
    make public the summary confluence page of the cluster upgrade

**Action - Damien**:
    Tag and release hdbpp-es / hdbpp-cm / libhdbpp / libhdbpp-timescale

**Action - All**:
    Share the tools which could be useful for the HDB++ community and update read the docs HDB++ section to advertise these tools.

**Action - All**: 
    Try the reader implementation in python3.

