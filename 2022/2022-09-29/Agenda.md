# HDB++ Teleconference Meeting

To be held on 2022/09/29 at 10:00 CET on zoom.

**Framapad**: https://semestriel.framapad.org/p/hdbpp-meeting-20220518-9u9j

**Participants**:   

**Next meeting date**:

# Minutes
    
## Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/hdbpp/meeting-minutes/-/blob/master/2022/2022-05-18/Minutes.md#news-on-recent-developments)

**Action - Sergi, Damien, Giacomo**:   
 Sergi will contact Damien and Giacomo to try to get a unified extraction API (python)  
        
**Action Graziano & Matteo**:
 Converge on a solution for a script to configure easily a bunch of attributes from a text configuration file and push this solution to tango-controls-hdbpp Github organization.
 MaxIV is using some template yml files to configure easily the archiving settings (event thresholds for specific classes, strategies)
    
**Action - Alba**:
 Provide MariaDB container(s) for the HDB++ benchmarking suite. Sergi will get in touch with Graziano on that topic.
    
**Action - cppTango team**: 
- Test the event reconnection issue when moving a device server instance from one host to another. Is this issue still present in recent cppTango development branches?
- Review and merge https://gitlab.com/tango-controls/cppTango/-/merge_requests/737 when ready
    
**Action - MaxIV**:
 Transfer the YAML tools to tango-controls Gitlab HDB++ subgroup to share them with the community and to help to get some feedback from the community.  
    
**Action - Sergi**:
 Create an issue in libhdbpp-mysql about the compilation problem on Debian 10.  

**Action - All**:
 Share the tools which could be useful for the HDB++ community and update read the docs HDB++ section to advertise these tools.

**Action - Reynald**:
 Finish migration to gitlab.

**Action - Sergi, Johan, Lucio**:
 For the june tango meeting, prepare a general talk and a short tour of the tools around hdb++ (Demo or Video Demo if possible):
  - python extraction.
  - eGiga
  - archwizard
  - ...

**Action - All**:
 Try the reader implementation in python3.

## News on recent developments

  - Fixed an issue on hdbpp-es. On errors on batch insertion retry was implemented in the backend and hdbpp-es, causing some errors.
  - At the ESRF, cluster update from postgresql-11/timescaledb-1.7 to postgresql-12/timescaledb-2.7.2
    - tedious process…
    - first update old cluster, using pg_upgrade for postgres and the normal procedure for timescale.
    - then integrate one of the machine of the new cluster.
    - switch to this machine as primary, and add the other machines as replicas.
    - drop the old cluster
    - a fast network is a good idea!

## AOB

 - hdbpp python reader ready for release ?
 - New release of hdbpp-es / hdbpp-cm / libhdbpp / libhdbpp-timescale? Current tag is 2 years old.
 - Publish package on conda-forge. Naming?

## Summary of remaining actions

