# HDB++ Teleconference Meeting

Held on 2023/12/05 at 3:00 pm CET on zoom.

**Framapad**: https://semestriel.framapad.org/p/hdbpp-meeting-20220518-9u9j

**Participants**:
  - Damien Lacoste (ESRF)
  - Reynald Bourtembourg (ESRF)
  - Lorenzo Pivetta (Elettra)
  - Thomas Juerges (SKAO)
  - Graziano Scalamera (Elettra)

**Next meeting date**:
February 6th 2024, 3pm CET. 
In person meeting April or May, to be combined with the documentation camp?

# Minutes

## Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/hdbpp/meeting-minutes/-/blob/master/2023/2023-05-24/Minutes.md#summary-of-remaining-actions)

**Action - All**:
- Test the sqlite backend
https://gitlab.com/tango-controls/hdbpp/libhdbpp-sqlite
Thomas J will give it a try.

**Action - All**:
- Work on a strategy to benchmark the different backends, and their versions.
https://gitlab.com/tango-controls/hdbpp/hdbpp-benchmark
No progress recently

**Action - cppTango team**:
- Review and merge https://gitlab.com/tango-controls/cppTango/-/merge_requests/737 when ready
Merged, but there is a similar bug on JTango.

**Action - Damien**:
make public the summary confluence page of the cluster upgrade

**Action - All**:
Share the tools which could be useful for the HDB++ community and update read the docs HDB++ section to advertise these tools.

**Action - All**:
Try the reader implementation in python3.
SKAO is starting to use it
Elettra as well, no feedback so far. Fix on timestamp was required to work with mysql
ESRF too, no feedback so far

## News on recent developments

- Release of all the stack for tango 9.4/5 support
It’s ok to release, minor and patch version without much discussion
Major version should go through RC before release
We have to put a CONTRIBUTING.md file or something similar to keep track of these decisions. @Damien

## AOB
Damien started to discuss with people doing archiving without tango.
There is a mailing list going on, and a slack channel.
Damien is to send an invite in the HDB++ group for people that are interested.
Gianluca Chiozzi showed interest in an engineer archiving system, maybe we can invite him as well.

## Summary of remaining actions 

**Action - All**:
- Test the sqlite backend
https://gitlab.com/tango-controls/hdbpp/libhdbpp-sqlite

**Action - All**:
- Work on a strategy to benchmark the different backends, and their versions.
https://gitlab.com/tango-controls/hdbpp/hdbpp-benchmark

**Action - JTango team**:
- Fix https://gitlab.com/tango-controls/JTango/-/issues/140

**Action - Damien**:
make public the summary confluence page of the cluster upgrade

**Action - All**:
Share the tools which could be useful for the HDB++ community and update read the docs HDB++ section to advertise these tools.

**Action - All**:
Get feedback on the reader implementation in python3.

**Action - Damien**:
Create a CONTRIBUTING.md file or something similar to describe the release process at least, and anything else that could be necessary.

**Action - Damien**:
Invite everyone to the global archiving collaboration.
Contact Gianluca Chiozzi on this subject.

