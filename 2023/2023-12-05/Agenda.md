# HDB++ Teleconference Meeting

To be held on 2023/12/05 at 3:00 pm CET on zoom.

**Framapad**: https://semestriel.framapad.org/p/hdbpp-meeting-20220518-9u9j

**Participants**:

**Next meeting date**:

# Minutes

## Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/hdbpp/meeting-minutes/-/blob/master/2023/2023-05-24/Minutes.md#summary-of-remaining-actions)

**Action - All**:
- Test the sqlite backend

**Action - All**:
- Work on a strategy to benchmark the different backends, and their versions.
https://gitlab.com/tango-controls/hdbpp/hdbpp-benchmark

**Action - cppTango team**:
- Review and merge https://gitlab.com/tango-controls/cppTango/-/merge_requests/737 when ready

**Action - Damien**:
    make public the summary confluence page of the cluster upgrade

**Action - All**:
    Share the tools which could be useful for the HDB++ community and update read the docs HDB++ section to advertise these tools.

**Action - All**:
    Try the reader implementation in python3.

## News on recent developments

  - Release of all the stack for tango 9.4/5 support

## AOB

  - Feedback from ICALEPCS 2023

## Summary of remaining actions
