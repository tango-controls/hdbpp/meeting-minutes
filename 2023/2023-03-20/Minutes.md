# HDB++ Teleconference Meeting

To be held on 2023/03/20 at 14:00 CET on zoom.

**Framapad**: https://semestriel.framapad.org/p/hdbpp-meeting-20220518-9u9j

**Participants**:
    - Damien Lacoste (ESRF)
    - Graziano Scalamera (ELETTRA)
    - Lorenzo Pivetta (ELETTRA)
    - Reynald Bourtembourg (ESRF)
    - Sergi Rubio (ALBA)
    - Thomas Juerges (SKAO)



**Next meeting date**:
Wednesday 24th of may 10 am
# Minutes

## Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/hdbpp/meeting-minutes/-/blob/master/2022/2022-11-16/Minutes.md#summary-of-remaining-actions)

**Action - Thomas Juerges**:
- Ask for some legal advice on the timescaleDB license.
we should be fine.
Thomas J shared the legal advice from SKAO with the group. Sorry, but the text cannot be shared with the public.

**Action - All**:
- 15 min SQLite backend challenge: Can we build an sqlite backend for hdbpp in less than 15 min?
Any contributions that took longer, but did it anyway will be accepted.

no news.

**Action - All**:
- Work on a strategy to benchmark the different backends, and their versions.
https://gitlab.com/tango-controls/hdbpp/hdbpp-benchmark

Could help to have an idea on what to do for the chunk size, for instance.
Current benchmark suite is missing an extracting tool, libhdbpp-python could fill this purpose.


**Action - cppTango team**:
- Test the event reconnection issue when moving a device server instance from one host to another. Is this issue still present in recent cppTango development branches?
- Review and merge https://gitlab.com/tango-controls/cppTango/-/merge_requests/737 when ready

no news but for the first part there might not be an issue...

**Action - Damien**:
    make public the summary confluence page of the cluster upgrade
Damien: Some problems with the aggregations. TimescaleDB chose to change the format of them. Dmaien tried a migration but it did not work out. Recommendation Do not migrate but start directly with the new format.

**Action - All**:
    Share the tools which could be useful for the HDB++ community and update read the docs HDB++ section to advertise these tools.

**Action - All**:
    Try the reader implementation in python3.
    merge multidb with maybe yaml file support
We need documentation!
Action Sergi: Will merge and provide a YAML example for TangoBox

## News on recent developments

  - Fixed various issues on hdbpp-es and libhdbpp-timescale
  - Issue to be discussed:
    - https://gitlab.com/tango-controls/hdbpp/hdbpp-es/-/issues/31
    What to do in case an attribute has its type changed? (or format or whatever)

## AOB

### Optimization of chunk size
Chunks can grow really big, we should be able to provide help on how to set it up properly.

### Action for next time:
    fix hdb++ documentation, links are pointing to github still.

### timescaledb
replace ttl col by interval instead of int

### cpptango 9.4 migration
cpptango 9.3 to 9.4 is changing the include path. it might requires some tricks to have projects easy to build against both versions, we have to get the projects 9.4 ready.
If I remember correctly, then Thomas Braun recommended as a quick fix to create a symlink named INSTALLATION_PATH/include/tango.h which points to INSTALLATION_PATH/include/tango/tnago.h

## Summary of remaining actions
**Action - All**:
- 15 min SQLite backend challenge: Can we build an sqlite backend for hdbpp in less than 15 min?
Any contributions that took longer, but did it anyway will be accepted.

**Action - All**:
- Work on a strategy to benchmark the different backends, and their versions.
https://gitlab.com/tango-controls/hdbpp/hdbpp-benchmark

**Action - cppTango team**:
- Review and merge https://gitlab.com/tango-controls/cppTango/-/merge_requests/737 when ready

**Action - Damien**:
    make public the summary confluence page of the cluster upgrade

**Action - All**:
    Share the tools which could be useful for the HDB++ community and update read the docs HDB++ section to advertise these tools.

**Action - All**:
    Try the reader implementation in python3.

**Action - Sergi**:
    Merge multidb branch on libhdbpp-python.
    Have it working with a yaml configuration file, and ready for tangobox.

**Action - All**:
    Fix links in the tango documentation to point to gitlab and not github.
