# HDB++ Teleconference Meeting

To be held on 2023/03/20 at 14:00 CET on zoom.

**Framapad**: https://semestriel.framapad.org/p/hdbpp-meeting-20220518-9u9j

**Participants**:

**Next meeting date**:

# Minutes

## Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/hdbpp/meeting-minutes/-/blob/master/2022/2022-11-16/Minutes.md#summary-of-remaining-actions)

**Action - Thomas Juerges**:
- Ask for some legal advice on the timescaleDB license.

**Action - All**:
- 15 min SQLite backend challenge: Can we build an sqlite backend for hdbpp in less than 15 min?
Any contributions that took longer, but did it anyway will be accepted.

**Action - All**:
- Work on a strategy to benchmark the different backends, and their versions.

**Action - cppTango team**:
- Test the event reconnection issue when moving a device server instance from one host to another. Is this issue still present in recent cppTango development branches?
- Review and merge https://gitlab.com/tango-controls/cppTango/-/merge_requests/737 when ready

**Action - Damien**:
    make public the summary confluence page of the cluster upgrade

**Action - All**:
    Share the tools which could be useful for the HDB++ community and update read the docs HDB++ section to advertise these tools.

**Action - All**:
    Try the reader implementation in python3.

## News on recent developments

  - Fixed various issues on hdbpp-es and libhdbpp-timescale
  - Issue to be discussed:
    - https://gitlab.com/tango-controls/hdbpp/hdbpp-es/-/issues/31
    What to do in case an attribute has its type changed? (or format or whatever)

## AOB

## Summary of remaining actions
