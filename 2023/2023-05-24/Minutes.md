# HDB++ Teleconference Meeting

Held on 2023/05/24 at 10:00 CEST on zoom.

**Meeting link**: https://esrf.zoom.us/j/94017863362?pwd=QWlMVzZWQTJyb1dPMk1OWmw0Z05SZz09

**Framapad**: https://semestriel.framapad.org/p/hdbpp-meeting-20220518-9u9j

**Participants**:
    Reynald Bourtembourg (ESRF)
    Lorenzo Pivetta (Elettra)
    Graziano Scalamera (Elettra)
    Sergi Rubio (ALBA)
    Johan Forsberg (MaxIV)
    Damien Lacoste (ESRF)
    Thomas Juerges (SKAO)

**Next meeting date**:
2023/06/21 at 10:00 CEST on zoom, to prepare tango community meeting
# Minutes

## Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/hdbpp/meeting-minutes/-/blob/master/2023/2023-03-20/Minutes.md#summary-of-remaining-actions)

**Action - All**:
- 15 min SQLite backend challenge: Can we build an sqlite backend for hdbpp in less than 15 min?
Any contributions that took longer, but did it anyway will be accepted.

An intern will join ESRF for 2 months, and work on this.

**Action - All**:
- Work on a strategy to benchmark the different backends, and their versions.
https://gitlab.com/tango-controls/hdbpp/hdbpp-benchmark
no news

**Action - cppTango team**:
- Review and merge https://gitlab.com/tango-controls/cppTango/-/merge_requests/737 when ready
reviewers needed

**Action - Damien**:
    make public the summary confluence page of the cluster upgrade
not done, there is an issue with aggregation.

**Action - All**:
    Share the tools which could be useful for the HDB++ community and update read the docs HDB++ section to advertise these tools.
Merge of pyhdbpp multi backend branch.
taurus plotting widget is improving
Sergi working on a tool to check that archiving is properly working and restore it to a working state if needed. (cron job starting a script every day)
Johan shared some work on grafana to collect some metrics. To be shared on the hdbpp-timescale-project monorepo

**Action - All**:
    Try the reader implementation in python3.

**Action - Sergi**:
    Merge multidb branch on libhdbpp-python.
    Have it working with a yaml configuration file, and ready for tangobox.
Merge was done
The yaml configuration is yet to be done

**Action - All**:
    Fix links in the tango documentation to point to gitlab and not github.
    https://gitlab.com/tango-controls/tango-doc/-/merge_requests/401
    done

## News on recent developments

  - Work on lihdbpp-timescale to support latest pqxx version (and work on mac)

## AOB

  - ICALEPCS contribution :   Abstract submitted (anexed at the end)
  To be done at the next meeting
  - Tango meeting contribution

  have a status report from each institute
   * some slides on pyhdbpp Sergi/Damien
   * some slides from MaxIV/grafana Johan
   * some slides from Elettra/eGiga Graziano
   * prepare a list of backends and clients Damien
   * some slides from SKAO about engineering data Thomas
   * maybe a small demo of the web clients, if the people are available

  draft to be checked wednesday 21 10am CEST
  Prepare a demo/howto on hdbpp for thursday morning

## Summary of remaining actions

**Action - Damien**:
    Release a new version of HDBEventSubscriber

**Action - All**:
    Try the reader implementation in python3.

**Action - Damien**:
    make public the summary confluence page of the cluster upgrade

**Action - Lisa**:
    SQLite backend development

**Action - cppTango team**:
    Review and merge https://gitlab.com/tango-controls/cppTango/-/merge_requests/737 when ready

**Action - All**:
    Work on a strategy to benchmark the different backends, and their versions.
https://gitlab.com/tango-controls/hdbpp/hdbpp-benchmark

**Action - Sergi**:
    Multidb support on libhdbpp-python.
    Have it working with a yaml configuration file, and ready for tangobox.

**Action - All institutes**:
    Prepare some slides for a presentation at the tango community meeting.
    Extra work for the people cited a bit before.

----

## ICALEPCS '23 Abstract (https://oraweb.cern.ch/pls/icalepcs2023/JACoW.edit_abstract?abs_id=1713)

Title:New developements on HDB++, the high-performance data archiving for Tango Controls
Presenter:Damien Lacoste - European Synchrotron Radiation Facility
Authors:Damien Lacoste, Reynald Bourtembourg (ESRF, Grenoble), Sergi Rubio-Manrique (ALBA-CELLS, Cerdanyola del Vallès), Jan David Mol (ASTRON, Dwingeloo), Lorenzo Pivetta, Graziano Scalamera (Elettra-Sincrotrone Trieste S.C.p.A., Basovizza), Johan Forsberg (MAX IV Laboratory, Lund), Thomas Juerges (SKAO, Macclesfield)

The Tango HDB++ project is a high performance event-driven archiving system which stores data with micro-second resolution timestamps. HDB++ supports many different backends, including MySQL/MariaDB, TimeScaleDB (a time-series PostgreSQL extension), and soon SQLite. Building on its flexible design, latest developments made supporting new backends even easier. HDB++ keeps improving with new features such as batch insertion and by becoming easier to install or setup in a testing environment, using ready to use docker images and striving to simplify all the steps of deployment. The HDB++ project is not only a data storage installation, but a full ecosystem to manage data, query it, and get the information needed. In this effort a lot of tools were developed to put a powerful backend to its proper use and be able to get the best out of the stored data. In this paper we will present as well the latest developments in data extraction, from low level libraries to web viewer integration such as grafana. Pointing out strategies in use in terms of data decimation, compression and others to help deliver data as fast as possible. Keywords: Tango, HDB++, Archiving, MySqL/MariaDB, TimeScaleDB
