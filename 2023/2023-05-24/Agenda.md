# HDB++ Teleconference Meeting

To be held on 2023/05/24 at 10:00 CEST on zoom.

**Framapad**: https://semestriel.framapad.org/p/hdbpp-meeting-20220518-9u9j

**Participants**:

**Next meeting date**:

# Minutes

## Status of [Actions defined in the previous meetings](https://gitlab.com/tango-controls/hdbpp/meeting-minutes/-/blob/master/2023/2023-03-20/Minutes.md#summary-of-remaining-actions)

**Action - All**:
- 15 min SQLite backend challenge: Can we build an sqlite backend for hdbpp in less than 15 min?
Any contributions that took longer, but did it anyway will be accepted.

**Action - All**:
- Work on a strategy to benchmark the different backends, and their versions.
https://gitlab.com/tango-controls/hdbpp/hdbpp-benchmark

**Action - cppTango team**:
- Review and merge https://gitlab.com/tango-controls/cppTango/-/merge_requests/737 when ready

**Action - Damien**:
    make public the summary confluence page of the cluster upgrade

**Action - All**:
    Share the tools which could be useful for the HDB++ community and update read the docs HDB++ section to advertise these tools.

**Action - All**:
    Try the reader implementation in python3.

**Action - Sergi**:
    Merge multidb branch on libhdbpp-python.
    Have it working with a yaml configuration file, and ready for tangobox.

**Action - All**:
    Fix links in the tango documentation to point to gitlab and not github.

## News on recent developments

  - Work on lihdbpp-timescale to support latest pqxx version (and work on mac)

## AOB

  - ICALEPCS contribution
  - Tango meeting contribution

## Summary of remaining actions
